const express = require('express')

const enterpriseController = require('../controllers/adminEnterprise')
const branchController = require('../controllers/adminBranch')
const userController = require('../controllers/adminUser')
const assetController = require('../controllers/adminAsset')

const router = express.Router()

router.post('/enterprise', enterpriseController.postEnterprise)

router.get('/enterprise/:id', enterpriseController.getEnterprise)
router.get('/enterprises', enterpriseController.getEnterprises)

router.patch('/enterprise/:id', enterpriseController.updateEnterprise)

router.delete('/enterprise/:id', enterpriseController.deleteEnterprise)

router.post('/branch', branchController.postBranch)

router.get('/branch/:id', branchController.getBranch)
router.get('/branches', branchController.getBranches)

router.patch('/branch/:id', branchController.updateBranch)

router.delete('/branch/:id', branchController.deleteBranch)

router.post('/user', userController.postUser)

router.get('/users', userController.getUsers)
router.get('/user/:id', userController.getUser)

router.patch('/user/:id', userController.updateUser)

router.delete('/user/:id', userController.deleteUser)

router.post('/asset', assetController.postAsset)

router.get('/assets', assetController.getAssets)
router.get('/asset/:id', assetController.getAsset)

router.patch('/asset/:id', assetController.updateAsset)

router.delete('/asset/:id', assetController.deleteAsset)

module.exports = router
