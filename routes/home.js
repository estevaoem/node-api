const path = require('path')

const express = require('express')

const router = express.Router()

router.get('/', (req, res, next) => {
  const filePath = path.join(__dirname, '../', 'views', 'home.html')
  res.sendFile(filePath)
})

module.exports = router
