const ObjectId = require('mongodb').ObjectId

const dbCollection = require('../util/config/mongo').collections.asset
const getDb = require('../util/config/mongo').getDb

class Asset {
  constructor (name, description, model, assignee, status, health, branch, imageUrl) {
    this.name = name
    this.description = description
    this.model = model
    this.assignee = assignee
    this.status = status
    this.health = health
    this.branch = branch
    this.imageUrl = imageUrl
  }

  insert () {
    const _db = getDb()
    return _db.collection(dbCollection)
      .insertOne(this)
  }

  static getAll () {
    const _db = getDb()
    return _db.collection(dbCollection)
      .find()
      .toArray()
  }

  static getOne (id) {
    const _db = getDb()
    return _db.collection(dbCollection)
      .findOne({
        _id: ObjectId(id)
      })
  }

  static update (id, patches) {
    const _db = getDb()
    return _db.collection(dbCollection)
      .updateOne(
        {
          _id: ObjectId(id)
        },
        {
          $set: patches
        })
  }

  static delete (id) {
    const _db = getDb()
    return _db.collection(dbCollection)
      .deleteOne({
        _id: ObjectId(id)
      })
  }
}

module.exports = Asset
