const ObjectId = require('mongodb').ObjectId

const dbCollection = require('../util/config/mongo').collections.branch
const getDb = require('../util/config/mongo').getDb

class Branch {
  constructor (name, city, enterprise) {
    this.name = name
    this.city = city
    this.enterprise = ObjectId(enterprise)
  }

  insert () {
    const _db = getDb()
    return _db.collection(dbCollection)
      .insertOne(this)
  }

  static getAll () {
    const _db = getDb()
    return _db.collection(dbCollection)
      .find()
      .toArray()
  }

  static getOne (id) {
    const _db = getDb()
    return _db.collection(dbCollection)
      .findOne({
        _id: ObjectId(id)
      })
  }

  static update (id, patches) {
    const _db = getDb()
    return _db.collection(dbCollection)
      .updateOne(
        {
          _id: ObjectId(id)
        },
        {
          $set: patches
        })
  }

  static delete (id) {
    const _db = getDb()
    return _db.collection(dbCollection)
      .deleteOne({
        _id: ObjectId(id)
      })
  }
}

module.exports = Branch
