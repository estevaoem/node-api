const Branch = require('../models/branch')

exports.getBranch = (req, res, next) => {
  const id = req.params.id
  Branch.getOne(id)
    .then(result => {
      const data = JSON.stringify(result)
      res.write(data)
      res.status(200)
      res.end()
    })
    .catch(err => {
      throw err
    })
}

exports.postBranch = (req, res, next) => {
  const name = req.body.name
  const city = req.body.city
  const enterprise = req.body.id
  const branch = new Branch(name, city, enterprise)
  branch
    .insert()
    .then(result => {
      res.status(201)
      res.end()
    })
    .catch(err => {
      throw err
    })
}

exports.getBranches = (req, res, next) => {
  Branch.getAll()
    .then(result => {
      const data = JSON.stringify(result)
      res.write(data)
      res.status(200)
      res.end()
    })
    .catch(err => {
      throw err
    })
}

exports.deleteBranch = (req, res, next) => {
  const id = req.params.id
  Branch.delete(id)
    .then(result => {
      res.status(204)
      res.end()
    })
    .catch(err => {
      throw err
    })
}

exports.updateBranch = (req, res, next) => {
  const patches = req.body
  const id = req.params.id
  Branch.update(id, patches)
    .then(result => {
      res.status(204)
      res.end()
    })
    .catch(err => {
      throw err
    })
}
