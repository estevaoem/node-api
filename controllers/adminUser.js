const User = require('../models/user')

exports.getUser = (req, res, next) => {
  const id = req.params.id
  User.getOne(id)
    .then(result => {
      const data = JSON.stringify(result)
      res.write(data)
      res.status(200)
      res.end()
    })
    .catch(err => {
      throw err
    })
}

exports.postUser = (req, res, next) => {
  const name = req.body.name
  const branch = req.body.branch
  const email = req.body.email
  const password = req.body.password
  const isAdmin = req.body.isAdmin
  const user = new User(name, branch, email, password, isAdmin)
  user
    .insert()
    .then(result => {
      res.status(201)
      res.end()
    })
    .catch(err => {
      throw err
    })
}

exports.getUsers = (req, res, next) => {
  User.getAll()
    .then(result => {
      const data = JSON.stringify(result)
      res.write(data)
      res.status(200)
      res.end()
    })
    .catch(err => {
      throw err
    })
}

exports.deleteUser = (req, res, next) => {
  const id = req.params.id
  User.delete(id)
    .then(result => {
      res.status(204)
      res.end()
    })
    .catch(err => {
      throw err
    })
}

exports.updateUser = (req, res, next) => {
  const patches = req.body
  const id = req.params.id
  User.update(id, patches)
    .then(result => {
      res.status(204)
      res.end()
    })
    .catch(err => {
      throw err
    })
}
