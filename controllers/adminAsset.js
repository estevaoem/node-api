const Asset = require('../models/asset')

exports.getAsset = (req, res, next) => {
  const id = req.params.id
  Asset.getOne(id)
    .then(result => {
      const data = JSON.stringify(result)
      res.write(data)
      res.status(200)
      res.end()
    })
    .catch(err => {
      throw err
    })
}

exports.postAsset = (req, res, next) => {
  const name = req.body.name
  const description = req.body.description
  const model = req.body.model
  const assignee = req.body.assignee
  const status = req.body.status
  const health = req.body.health
  const branch = req.body.branch
  const imageUrl = req.file.path
  const asset = new Asset(name, description, model, assignee, status, health, branch, imageUrl)
  asset
    .insert()
    .then(result => {
      res.status(201)
      res.end()
    })
    .catch(err => {
      throw err
    })
}

exports.getAssets = (req, res, next) => {
  Asset.getAll()
    .then(result => {
      const data = JSON.stringify(result)
      res.write(data)
      res.status(200)
      res.end()
    })
    .catch(err => {
      throw err
    })
}

exports.deleteAsset = (req, res, next) => {
  const id = req.params.id
  Asset.delete(id)
    .then(result => {
      res.status(204)
      res.end()
    })
    .catch(err => {
      throw err
    })
}

exports.updateAsset = (req, res, next) => {
  const patches = req.body
  const id = req.params.id
  Asset.update(id, patches)
    .then(result => {
      res.status(204)
      res.end()
    })
    .catch(err => {
      throw err
    })
}
