const Enterprise = require('../models/enterprise')

exports.getEnterprise = (req, res, next) => {
  const id = req.params.id
  Enterprise.getOne(id)
    .then(result => {
      const data = JSON.stringify(result)
      res.write(data)
      res.status(200)
      res.end()
    })
    .catch(err => {
      throw err
    })
}

exports.postEnterprise = (req, res, next) => {
  const name = req.body.name
  const slogan = req.body.slogan
  const enterprise = new Enterprise(name, slogan)
  enterprise
    .insert()
    .then(result => {
      res.status(201)
      res.end()
    })
    .catch(err => {
      throw err
    })
}

exports.getEnterprises = (req, res, next) => {
  Enterprise.getAll()
    .then(result => {
      const data = JSON.stringify(result)
      res.write(data)
      res.status(200)
      res.end()
    })
    .catch(err => {
      throw err
    })
}

exports.deleteEnterprise = (req, res, next) => {
  const id = req.params.id
  Enterprise.delete(id)
    .then(result => {
      res.status(204)
      res.end()
    })
    .catch(err => {
      throw err
    })
}

exports.updateEnterprise = (req, res, next) => {
  const patches = req.body
  const id = req.params.id
  Enterprise.update(id, patches)
    .then(result => {
      res.status(204)
      res.end()
    })
    .catch(err => {
      throw err
    })
}
