const bodyParser = require('body-parser')
const express = require('express')
const multer = require('multer')

const adminRoutes = require('./routes/admin')
const homeRoute = require('./routes/home')

const dbConnect = require('./util/config/mongo').dbConnect

const fileStorage = require('./util/config/multer').fileStorage
const PORT = process.env.PORT || 3000

const app = express()

app.use(multer(
  {
    storage: fileStorage
  }
).single('imageUrl'))

app.use(bodyParser.urlencoded({ extended: false }))

app.use('/admin', adminRoutes)
app.use(homeRoute)

app.use((req, res, next) => {
  res.status(404)
  res.end()
})

dbConnect(() => {
  app.listen(PORT)
})
