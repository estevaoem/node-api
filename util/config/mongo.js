const mongodb = require('mongodb')

const dbName = 'data'
const dbPassword = 'secret-password'

const dbServerUrl = `mongodb://127.0.0.1:27017/${dbName}?retryWrites=true&w=majority`

const MongoClient = mongodb.MongoClient

let _db

const dbConnect = callback => {
  MongoClient.connect(dbServerUrl, { useUnifiedTopology: true })
    .then(client => {
      console.log('Database connected successfully')
      _db = client.db()
      callback()
    })
    .catch(err => {
      console.log('Error connecting to database')
      throw err
    })
}

const getDb = () => {
  if (_db) {
    return _db
  } else {
    console.log('No database on use, please check the config file')
  }
}

const collections = {
  asset: 'asset',
  branch: 'branch',
  enterprise: 'enterprise',
  user: 'user'
}

module.exports = {
  dbConnect: dbConnect,
  getDb: getDb,
  collections: collections
}
