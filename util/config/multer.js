const multer = require('multer')

const fileStorage = multer.diskStorage(
  {
    destination: (req, file, cb) => {
      cb(null, './media')
    },
    filename: (req, file, cb) => {
      cb(null, Date.now() + '_' + file.originalname)
    }
  }
)

module.exports = {
  fileStorage: fileStorage
}
