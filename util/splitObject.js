const splitObject = (obj, keys) => {
  const holder = {}
  keys.forEach(value => {
    holder[value] = obj[value]
  })
  return holder
}

module.exports = splitObject
