const patchFilter = patches => {
  const patchesKeys = Object.keys(patches)
  const patchesValues = Object.values(patches)
  const holder = {}
  for (let i = 0; i < patchesKeys.length; i++) {
    holder[`branches.$.${patchesKeys[i]}`] = patchesValues[i]
  }
  return holder
}

module.exports = patchFilter
